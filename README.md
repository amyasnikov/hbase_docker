# Docker example

* Author: Alexander Myasnikov
* mailto: myasnikov.alexander.s@gmail.com
* git: https://gitlab.com/amyasnikov/hbase_docker
* site: http://amyasnikov.pro



# Building

* Build server: ``` docker build --target hbase_server -t hbase_server -f Dockerfile_hbase . ```
* Build client: ``` docker build --target hbase_client -t hbase_client -f Dockerfile_hbase . ```
* Create network: ``` docker network create --driver bridge hbase_net ```



# Running

* Run server: ``` docker run --network hbase_net hbase_server ```
* Run client: ``` docker run --network hbase_net -e HBASE_IP=172.19.0.2 hbase_client ```
    * Where ```HBASE_IP``` show in: ``` docker network inspect bridge hbase_net ```


